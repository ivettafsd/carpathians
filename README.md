# Carpathians

Landing page for the 'Carpathians'
[Посилання на живу сторінку](https://ivetta-carpathians.netlify.app/)

Всі матеріали до проєкту:
[Макет](https://www.figma.com/file/GfkEVfxt873QcoGf8tgtVv/%D0%A2%D1%83%D1%80%D0%B8-%D0%B7%D1%96-%D0%9B%D1%8C%D0%B2%D0%BE%D0%B2%D0%B0-%D1%83-%D0%9A%D0%B0%D1%80%D0%BF%D0%B0%D1%82%D0%B8?type=design&node-id=3-3448&t=trhB0WB3Hf9Ikp66-0)
[Технічне завдання](https://docs.google.com/document/d/19C142cqvbv-jF1uNAlV2MuBZKb4JQQmI_rmmJSjkp5Y/edit)