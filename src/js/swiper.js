import Swiper from 'swiper/bundle';

import 'swiper/css/bundle';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

export const buildSwiperSlider = sliderElm => {
  const sliderIdentifier = sliderElm;
  const swiper = new Swiper(`[data-id="${sliderIdentifier}"]`, {
    slidesPerView: 1,
    spaceBetween: 64,
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      1440: {
        slidesPerView: 3,
        spaceBetween: 20,
      },
    },
    navigation: {
      nextEl: `.swiper-button-next-${sliderIdentifier}`,
      prevEl: `.swiper-button-prev-${sliderIdentifier}`,
    },
  });
  return swiper;
};

buildSwiperSlider('tours');

export const buildSwiperSliderGallery = sliderElm => {
  const sliderIdentifier = sliderElm;
  const swiper = new Swiper(`[data-id="${sliderIdentifier}"]`, {
    slidesPerView: 'auto',
    spaceBetween: 20,
    breakpoints: {
      768: {
        slidesPerGroup: 2,
      },
      1440: {
        slidesPerGroup: 2,
      },
    },
    navigation: {
      nextEl: `.swiper-button-next-${sliderIdentifier}`,
      prevEl: `.swiper-button-prev-${sliderIdentifier}`,
    },
  });
  return swiper;
};
buildSwiperSliderGallery('gallery');

